# mTCP (UPX)

A collection of UPX compressed TCP/IP tools: DHCP, IRC, FTP, Telnet, Netcat, HTGet, Ping, SNTP. This package does not include HTTPServ or Netdrive.

# Contributing

#### Although included with FreeDOS, mTCP is separate project that was created and is actively maintained by Michael B. Brutman.
#### For more information regarding mTCP or to suggest changes, please visit the [mTCP Homepage](http://www.brutman.com/mTCP/) and contact the author directly.

## MTCP.LSM

<table>
<tr><td>title</td><td>mTCP (UPX)</td></tr>
<tr><td>version</td><td>2024-10-20 (FreeDOS)</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-10-20</td></tr>
<tr><td>description</td><td>A collection of TCP/IP tools</td></tr>
<tr><td>summary</td><td>A collection of UPX compressed TCP/IP tools: DHCP, IRC, FTP, Telnet, Netcat, HTGet, Ping, SNTP. This package does not include HTTPServ or Netdrive.</td></tr>
<tr><td>keywords</td><td>dos tcp/ip realmode dhcp ftp telnet htget irc ircjr netcat sntp ping opensource 16 bit</td></tr>
<tr><td>author</td><td>Michael B. Brutman &lt;mbbrutman _AT_ gmail.com&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Michael B. Brutman &lt;mbbrutman _AT_ gmail.com&gt;</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.brutman.com/mTCP/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 3](LICENSE)</td></tr>
</table>
