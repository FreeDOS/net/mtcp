# Contributing

#### Although included with FreeDOS, mTCP is separate project that was created and is actively maintained by Michael B. Brutman.
#### For more information regarding mTCP or to suggest changes, please visit the [mTCP Homepage](http://www.brutman.com/mTCP/) and contact the author directly.
